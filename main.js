var eventBus = new Vue()


// Our components 

// Product component 
Vue.component('product', {
    props: {
        premium: {
            type: Boolean,
            required: true
        }
    },
    template: `
            <div class="product">

            <div class="product-image">
                <img v-bind:src="image" v-bind:alt="altText" />
            </div>

            <div class="product-info">

                <h1> {{ title }} </h1>
                <p v-if="inventory > 10">In Stock </p>
                <p v-else-if="inventory <= 10 && inventory > 0">Almost out of Stock! </p>
                <p v-else>Out of Stock </p>
                <p>Shipping : {{ shipping }} </p>

                <ul>
                    <li v-for="detail in details"> {{ detail }} </li>
                </ul>

                <div v-for="(variant,index) in variants" :key="variant.variantId" class="color-box" :style="{ backgroundColor:variant.variantColor }" @mouseover="updateProduct(index)">
                </div>

                <button v-on:click="addToCart" :disabled="!inStock" :class="{ disabledButton: !inStock }">Add to Cart</button>

                <button v-on:click="removeFromCart">Remove from Cart</button>

                <product-tabs :reviews="reviews"></product-tabs>

            </div>

        </div>
    
    `,
    data() {
        return {
            product: 'Socks',
            brand: 'Louis Vutton',
            selectedVariant: 0,
            altText: 'A pair of Socks.',
            details: ["80% Cotton", "20% Polyester", "Gender Neutral"],
            variants: [{
                variantId: 2234,
                variantColor: 'green',
                variantImage: './assets/img/vmSocks-green-onWhite.jpg',
                variantQuantity: 10,
            }, {
                variantId: 2235,
                variantColor: 'blue',
                variantImage: './assets/img/vmSocks-green-onBlue.jpg',
                variantQuantity: 0,
            }],
            reviews: []
        }
    },

    methods: {
        addToCart() {
            this.$emit('add-to-cart', this.variants[this.selectedVariant].variantId)
        },

        removeFromCart() {
            this.$emit('remove-from-cart', this.variants[this.selectedVariant].variantId)
        },

        addReview(productReview) {
            this.reviews.push(productReview)
        }

    },
    computed: {
        title() {
            return this.brand + ' ' + this.product
        },
        image() {
            return this.variants[this.selectedVariant].variantImage
        },
        inventory() {
            return this.variants[this.selectedVariant].variantQuantity
        },
        inStock() {
            return this.variants[this.selectedVariant].variantQuantity
        },
        shipping() {
            if (this.premium) {
                return "Free"
            }
            return 2.99
        }
    },
    mounted() {
        eventBus.$on('review-submitted', productReview => {
            this.reviews.push(productReview)
        })
    }
})

// Product review component 
Vue.component('product-review', {
    template: `
       <form class="review-form" @submit.prevent="onSubmit">

       <p v-if="errors.length">
            <b>Please correct the following error(s):</b>
            <ul>
                <li v-for="error in errors"> {{ error }} </li>
            </ul>
       </p>

       <p>
            <label for="name">Name:</label>
            <input id="name" v-model="name">
       </p>  

       <p>
            <label for="review">Review:</label>
            <textarea id="name" v-model="review"></textarea>
       </p>  

       <p>
            <label for="rating">Rating:</label>
            <select id="rating" v-model.number="rating">
                <option>5</option>
                <option>4</option>
                <option>3</option>
                <option>2</option>
                <option>1</option>                
            </select>
        </p>  

        <p>
            <input type="submit" value="Submit">
        </p>

       </form>
    `,
    data() {
        return {
            name: null,
            review: null,
            rating: null,
            errors: []
        }
    },
    methods: {
        onSubmit() {
            if (this.name && this.review && this.rating) {

                let productReview = {
                    name: this.name,
                    review: this.review,
                    rating: this.rating
                }

                eventBus.$emit('review-submitted', productReview)

                // Reset values evertime the form is submitted                 
                this.name = null
                this.review = null
                this.rating = null
                this.errors = []
            } else {
                this.errors = []
                if (!this.name) this.errors.push("Name required.")
                if (!this.review) this.errors.push("Review required.")
                if (!this.rating) this.errors.push("Rating required.")
            }
        }
    }
})

Vue.component('product-tabs', {
    props: {
        reviews: {
            type: Array,
            required: true
        }
    },
    template: `
        <div>
            <span class="tab" 
                :class="{ activeTab: selectedTab === tab }" 
                v-for="(tab, index) in tabs"  
                :key="index" 
                @click="selectedTab = tab"> 
                {{ tab }}                  
                </span>

                <div v-show="selectedTab === 'Reviews'">
                    <h2>Reviews</h2>
                    <p v-if="!reviews.length">There are no reviews yet.<p>
                    
                    <ul>
                        <li v-for="review in reviews"> {{ review.name }} | Rating : {{ review.rating}}, Review : {{ review.review}} </li>
                    </ul>
                </div>

                <br/><br/>
                Product Review <br/>
                <product-review v-show="selectedTab === 'Make a Review'" ></product-review>
                <br/>
                
        </div>
    `,
    data() {
        return {
            tabs: ['Reviews', 'Make a Review'],
            selectedTab: "Reviews"
        }
    }
})

var app = new Vue({
    el: '#app',
    data: {
        premium: true,
        cart: []
    },
    methods: {

        addToCart(id) {
            this.cart.push(id)
        },

        removeFromCart(id) {
            this.cart.pop(id)
        },

    }

})


var service = new Vue({
    el: '#service',
    data: {
        service: 'Software'
    }
})